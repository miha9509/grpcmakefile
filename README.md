# grpcMakefile

```makefile
:PHONY
:SILENT

generate: ## generate proto files
	protoc --go_out=. --go-grpc_opt=require_unimplemented_servers=false --go-grpc_out=. internalApi/server.proto
	go mod tidy

protoinstall: ## install protobuff
	go get -u google.golang.org/protobuf/cmd/protoc-gen-go
	go install google.golang.org/protobuf/cmd/protoc-gen-go

	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc

protopaths: ## install protobuff paths for generate (on mac paste this in terminal or into bash.rc)
	export GOPATH=$HOME/go
	export PATH=$GOPATH/bin:$PATH

build: ## build application
	go build -o ./bin -ldflags "-w -s -X main.built=`date -u +%Y%m%d.H%M%S` -X main.commit=`git rev-parse --short HEAD`" -v cmd/main.go

run: ## build and run application
	make build
	cd bin && ./main ../etc/config.yaml

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
```
